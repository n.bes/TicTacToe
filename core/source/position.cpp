#include "position.h"

int toInt(Position position){
	switch (position) {
		case Position::A1:
			return 1;
		case Position::A2:
			return 2;
		case Position::A3:
			return 3;
		case Position::B1:
			return 4;
		case Position::B2:
			return 5;
		case Position::B3:
			return 6;
		case Position::C1:
			return 7;
		case Position::C2:
			return 8;
		case Position::C3:
			return 9;
		default:
			throw std::invalid_argument ("unknown position");
	}
}

Position fromInt(int value){
	switch (value) {
		case 1:
			return Position::A1;
		case 2:
			return Position::A2;
		case 3:
			return Position::A3;
		case 4:
			return Position::B1;
		case 5:
			return Position::B2;
		case 6:
			return Position::B3;
		case 7:
			return Position::C1;
		case 8:
			return Position::C2;
		case 9:
			return Position::C3;
		default:
			throw std::invalid_argument ("unknown value");
	}
}
