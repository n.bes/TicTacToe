#include "jsonMessageFactory.h"

nlohmann::json JsonMessageFactory::loginMsg(Team team) const{
	nlohmann::json json;
	json["type"] = "login";
	json["team"] = toStr(team);
	return json;
};

nlohmann::json JsonMessageFactory::moveMsg(Position position) const{
	nlohmann::json json;
	json["type"] = "move";
	json["position"] = toInt(position);
	return json;
};

nlohmann::json JsonMessageFactory::stateMsg(ServerState state) const{
	nlohmann::json json;
	json["type"] = "state";
	switch (state) {
		case ServerState::login:
			json["state"] = "login";
			break;
		case ServerState::userMove:
			json["state"] = "userMove";
			break;
		case ServerState::aiMove:
			json["state"] = "aiMove";
			break;
		case ServerState::gameOver:
			json["state"] = "gameOver";
			break;
		default:
			json["state"] = "uknown";
			break;
	}
	return json;
};

nlohmann::json JsonMessageFactory::fieldMsg(Field field) const{
	auto allPositions = {
		Position::A1, Position::A2,	Position::A3,
		Position::B1, Position::B2, Position::B3,
		Position::C1, Position::C2,	Position::C3
	};

	std::vector <std::string> positions;
	for (const auto &pos: allPositions){
		Team team = field.at(pos);
		std::string v = toStr(team);
		positions.push_back(v);
	}

	nlohmann::json json;
	json["type"] = "field";
	json["field"] = positions;
	return json;
};
