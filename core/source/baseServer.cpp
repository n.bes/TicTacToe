#include "baseServer.h"

ServerState BaseServer::getState(){
	return state;
};

void BaseServer::addMessage(std::string message){
	MessageFactory mf;
	auto m = mf.fromString(message);
	while (!messageQueue.push(m));
};

void BaseServer::tick(){
	switch (state) {
		case ServerState::login:
			onLogin();
			break;
		case ServerState::userMove:
			onUserMove();
			break;
		case ServerState::aiMove:
			onAiMove();
			break;
		case ServerState::gameOver:
			break;
		case ServerState::error:
			break;
		default:
			break;
	}
};

void BaseServer::sendField(){
	auto msg = jsonMsgFactory.fieldMsg(field);
	onMsg(msg.dump());
}
void BaseServer::sendState(){
	auto msg = jsonMsgFactory.stateMsg(state);
	onMsg(msg.dump());
}

void BaseServer::onLogin(){
	try{
		if (messageQueue.empty()){
			return;
		}

		std::shared_ptr<MessageInterface> message;
		while (!messageQueue.pop(message));

		if (auto loginMessage = std::dynamic_pointer_cast<LoginMessage>(message)){
			Team userTeam = loginMessage->getTeam();
			if (userTeam == Team::x){
				this->userTeam = userTeam;
				this->aiTeam = Team::o;
				state = ServerState::userMove;
			}
			else if (userTeam == Team::o){
				this->userTeam = userTeam;
				this->aiTeam = Team::x;

				state = ServerState::aiMove;
			}
			else{
				state = ServerState::error;
			}
			sendField();
			sendState();
		}
	}
	catch(...){
		state = ServerState::error;
	}
};
void BaseServer::onAiMove(){
	Position position = ai->move(field);
	if (field.at(position) != Team::unknown){
		state = ServerState::error;
	}
	else{
		field.set(aiTeam, position);
		sendField();

		bool weHaveWinner = field.getWinner() != Team::unknown;
		if (field.isFull() || weHaveWinner){
			state = ServerState::gameOver;
		}
		else{
			state = ServerState::userMove;
		}
		sendState();
	}
}

void BaseServer::onUserMove(){
	try{
		if (messageQueue.empty()){
			return;
		}

		std::shared_ptr<MessageInterface> message;
		while (!messageQueue.pop(message));

		if (auto moveMessage = std::dynamic_pointer_cast<MoveMessage>(message)){
			Position position = moveMessage->getPosition();

			if (field.at(position) == Team::unknown){
				field.set(userTeam, position);
				sendField();

				bool weHaveWinner = field.getWinner() != Team::unknown;
				if (field.isFull() || weHaveWinner){
					state = ServerState::gameOver;
				}
				else{
					state = ServerState::aiMove;
				}
			}
			sendState();
		}
	}
	catch(...){
		state = ServerState::error;
	}
}
Team BaseServer::getUserTeam(){
	return userTeam;
};

Team BaseServer::getAiTeam(){
	return aiTeam;
};

BaseServer::BaseServer(std::shared_ptr<AiInterface> ai){
	this->ai = ai;
}
