#include "linearAi.h"

Position LinearAi::move (Field field){
	auto allPosition = {
		Position::A1,
		Position::A2,
		Position::A3,
		Position::B1,
		Position::B2,
		Position::B3,
		Position::C1,
		Position::C2,
		Position::C3
	};
	for (const auto& pos: allPosition){
		if (field.at(pos) == Team::unknown){
			return pos;
		}
	}
	return Position::unknown;
}
