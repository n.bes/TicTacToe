#include "team.h"

std::string toStr(Team team){
	switch (team) {
		case Team::x:
			return "x";
		case Team::o:
			return "o";
		case Team::unknown:
			return "-";
		default:
			throw std::invalid_argument ("unknown team");
	}
};
