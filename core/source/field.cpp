#include "field.h"

Field::Field(){
	for (auto &v:array){
		v = Team::unknown;
	}
}

Team Field::at(Position position){
	int pos = toInt(position) - 1;
	return array[pos];
}

void Field::set(Team team, Position position){
	int pos = toInt(position) - 1;
	array[pos] = team;
}

bool Field::isFull(){
	for (const auto &v : array){
		if (v == Team::unknown){
			return false;
		}
	}
	return true;
}
Team Field::getWinner(){
	auto a1 = array[0];
	auto a2 = array[1];
	auto a3 = array[2];

	auto b1 = array[3];
	auto b2 = array[4];
	auto b3 = array[5];

	auto c1 = array[6];
	auto c2 = array[7];
	auto c3 = array[8];

	//rows
	if (a1 == a2 && a2 == a3){
		return a1;
	}

	if (b1 == b2 && b2 == b3){
		return b1;
	}

	if (c1 == c2 && c2 == c3){
		return c1;
	}

	//column
	if (a1 == b1 && b1 == c1){
		return a1;
	}

	if (a2 == b2 && b2 == c2){
		return a2;
	}

	if (a3 == b3 && b3 == c3){
		return a3;
	}

	//diagonal
	if (a1 == b2 && b2 == c3){
		return a1;
	}

	if (c1 == b2 && b2 == a3){
		return c1;
	}
	return Team::unknown;
}
