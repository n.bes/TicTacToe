#include "moveMessage.h"

MessageType MoveMessage::getType(){
	return MessageType::move;
}

void MoveMessage::checkType(nlohmann::json json){
	auto findTypeResult = json.find("type");
	if (findTypeResult == json.end()) {
		throw std::invalid_argument ("type not found");
	}

	if (!findTypeResult->is_string()){
		throw std::invalid_argument ("type is not string");
	}

	std::string type = findTypeResult->get<std::string>();
	if (type != "move"){
		throw std::invalid_argument ("type is not \"login\"");
	}
}

void MoveMessage::setPosition(nlohmann::json json){
	auto findTeamResult = json.find("position");
	if (findTeamResult == json.end()) {
		throw std::invalid_argument ("type not found");
	}

	if (!findTeamResult->is_number()){
		throw std::invalid_argument ("type is not string");
	}

	int pos = findTeamResult->get<int>();
	position = fromInt(pos);
}

MoveMessage::MoveMessage(std::string string){
	nlohmann::json json = nlohmann::json::parse(string);
	if (!json.is_object()){
		throw std::invalid_argument ("json root is not object");
	}

	checkType(json);
	setPosition(json);
}

Position MoveMessage::getPosition(){
	return position;
}
