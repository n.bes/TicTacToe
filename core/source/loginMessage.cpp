#include "loginMessage.h"

MessageType LoginMessage::getType(){
	return MessageType::login;
}

void LoginMessage::checkType(nlohmann::json json){
	auto findTypeResult = json.find("type");
	if (findTypeResult == json.end()) {
		throw std::invalid_argument ("type not found");
	}

	if (!findTypeResult->is_string()){
		throw std::invalid_argument ("type is not string");
	}

	std::string type = findTypeResult->get<std::string>();
	if (type != "login"){
		throw std::invalid_argument ("type is not \"login\"");
	}
}

void LoginMessage::setTeam(nlohmann::json json){
	auto findTeamResult = json.find("team");
	if (findTeamResult == json.end()) {
		throw std::invalid_argument ("type not found");
	}

	if (!findTeamResult->is_string()){
		throw std::invalid_argument ("type is not string");
	}

	std::string teamFromMessage = findTeamResult->get<std::string>();
	if (teamFromMessage == "x"){
		team = Team::x;
	}
	else if (teamFromMessage == "o"){
		team = Team::o;
	}
	else{
		throw std::invalid_argument ("unknown team");
	}
}

LoginMessage::LoginMessage(std::string string){
	nlohmann::json json = nlohmann::json::parse(string);
	if (!json.is_object()){
		throw std::invalid_argument ("json root is not object");
	}

	checkType(json);
	setTeam(json);
}


Team LoginMessage::getTeam(){
	return team;
}
