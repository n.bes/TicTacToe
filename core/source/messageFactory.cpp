#include "messageFactory.h"

std::shared_ptr<MessageInterface> MessageFactory::fromString(std::string string) const{
	try {
		nlohmann::json json = nlohmann::json::parse(string);

		if (!json.is_object()){
			return std::make_shared<UnknownMessage>(string);
		}

		auto result = json.find("type");
		if (result == json.end()) {
			return std::make_shared<UnknownMessage>(string);
		}

		if (!result->is_string()){
			return std::make_shared<UnknownMessage>(string);
		}

		std::string type = result->get<std::string>();
		if (type == "login"){
			return std::make_shared<LoginMessage>(string);
		}
		if (type == "move"){
			return std::make_shared<MoveMessage>(string);
		}
	}
	catch(const std::invalid_argument &invalid){
		return std::make_shared<UnknownMessage>(string);
	}
}
