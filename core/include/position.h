#pragma once
#include <stdexcept>
enum class Position{
	A1,
	A2,
	A3,
	B1,
	B2,
	B3,
	C1,
	C2,
	C3,
	unknown
};

int toInt(Position position);
Position fromInt(int value);
