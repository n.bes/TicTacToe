#pragma once
#include "field.h"
#include "position.h"
#include "team.h"
#include "serverState.h"

#include <json.hpp>

struct JsonMessageFactory{
	nlohmann::json loginMsg(Team team) const;
	nlohmann::json moveMsg(Position position) const;
	nlohmann::json fieldMsg(Field field) const;
	nlohmann::json stateMsg(ServerState state) const;
};
