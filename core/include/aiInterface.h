#pragma once
#include "position.h"
#include "field.h"

class AiInterface{
public:
	virtual Position move (Field field) = 0;
};
