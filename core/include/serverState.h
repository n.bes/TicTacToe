#pragma once
enum class ServerState{
	login,
	userMove,
	aiMove,
	gameOver,
	error
};
