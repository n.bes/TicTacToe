#pragma once
#include "messageInterface.h"
#include "position.h"

#include <json.hpp>

class MoveMessage : public MessageInterface{
	Position position;
	void checkType(nlohmann::json json);
	void setPosition(nlohmann::json json);
public:
	MoveMessage(std::string json);
	Position getPosition();
	MessageType getType();
};
