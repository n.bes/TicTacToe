#pragma once
#include "aiInterface.h"
#include "field.h"
#include "messageFactory.h"
#include "jsonMessageFactory.h"
#include "position.h"
#include "serverState.h"
#include "team.h"

#include <atomic>
#include <boost/lockfree/spsc_queue.hpp>
#include <boost/signals2.hpp>
#include <iostream>
#include <string>

class BaseServer{
	boost::lockfree::spsc_queue <std::shared_ptr<MessageInterface>,boost::lockfree::capacity<1024>> messageQueue;
	Field field;
	JsonMessageFactory jsonMsgFactory;

	std::atomic<ServerState> state {ServerState::login};
	std::shared_ptr<AiInterface> ai;

	Team aiTeam = Team::unknown;
	Team userTeam = Team::unknown;

	void onLogin();
	void onUserMove();
	void onAiMove();

	void sendField();
	void sendState();
public:
	boost::signals2::signal <void(std::string)> onMsg;
	BaseServer(std::shared_ptr<AiInterface> ai);
	Team getUserTeam();
	Team getAiTeam();

	void addMessage(std::string);
	ServerState getState();
	void tick();
};
