#pragma once
#include "messageInterface.h"
#include "loginMessage.h"
#include "unknownMessage.h"
#include "moveMessage.h"

#include <json.hpp>

#include <memory>

struct MessageFactory{
	std::shared_ptr<MessageInterface> fromString(std::string string) const;
};
