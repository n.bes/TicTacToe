#pragma once
#include "messageInterface.h"
#include <memory>

enum class MessageType{
	login,
	move,
	unknown
};

class MessageInterface{
public:
	virtual MessageType getType() = 0;
};
