#pragma once
#include "messageInterface.h"

#include <json.hpp>

class UnknownMessage : public MessageInterface{
public:
	UnknownMessage(std::string json);
	MessageType getType();
};
