#pragma once
#include "messageInterface.h"
#include "team.h"

#include <json.hpp>

class LoginMessage : public MessageInterface{
	Team team;
	void checkType(nlohmann::json json);
	void setTeam(nlohmann::json json);
public:
	LoginMessage(std::string json);
	Team getTeam();
	MessageType getType();
};
