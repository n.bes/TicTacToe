#pragma once
#include "team.h"
#include "position.h"

#include <array>

class Field{
	static const int size = 9;
	std::array <Team, size> array;
public:
	Field();
	Team at(Position position);
	void set(Team team, Position position);
	bool isFull();
	Team getWinner();
};
