#pragma once
#include <stdexcept>
#include <string>

enum class Team{
	x,
	o,
	unknown
};

std::string toStr(Team team);
