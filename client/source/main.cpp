#include <client_ws.hpp>
#include <client_wss.hpp>

int main() {
	using namespace std;
	typedef SimpleWeb::SocketClient<SimpleWeb::WS> WsClient;
	typedef SimpleWeb::SocketClient<SimpleWeb::WSS> WssClient;


	WsClient client("echo.websocket.org/");
	client.onmessage=[&client](shared_ptr<WsClient::Message> message) {
		auto message_str=message->string();
		cout << "Client: Message received: \"" << message_str << "\"" << endl;
		cout << "Client: Sending close connection" << endl;
		client.send_close(1000);
	};

	client.onopen=[&client]() {
		cout << "Client: Opened connection" << endl;
		string message="Hello";
		cout << "Client: Sending message: \"" << message << "\"" << endl;

		auto send_stream=make_shared<WsClient::SendStream>();
		*send_stream << message;
		client.send(send_stream);
	};

	client.onclose=[](int status, const string& reason) {
		cout << "Client: Closed connection with status code " << status << endl;
	};

	client.onerror=[](const boost::system::error_code& ec) {
		cout << "Client: Error: " << ec << ", error message: " << ec.message() << endl;
	};

	client.start();














	WssClient clientSecure("echo.websocket.org/", false);
	clientSecure.onmessage=[&clientSecure](shared_ptr<WssClient::Message> message) {
		auto message_str=message->string();

		cout << "Client: Message received: \"" << message_str << "\"" << endl;

		cout << "Client: Sending close connection" << endl;
		clientSecure.send_close(1000);
	};

	clientSecure.onopen=[&clientSecure]() {
		cout << "Client: Opened connection" << endl;

		string message="Hello";
		cout << "Client: Sending message: \"" << message << "\"" << endl;

		auto send_stream=make_shared<WssClient::SendStream>();
		*send_stream << message;
		clientSecure.send(send_stream);
	};

	clientSecure.onclose=[](int status, const string& reason) {
		cout << "Client: Closed connection with status code " << status << endl;
	};

	//See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
	clientSecure.onerror=[](const boost::system::error_code& ec) {
		cout << "Client: Error: " << ec << ", error message: " << ec.message() << endl;
	};

	clientSecure.start();

	return 0;
}
