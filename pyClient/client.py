import websocket
import thread
import time
import json
import sys

def getLoginMsg(team):
	return json.dumps({
		"type" : "login",
		"team" : team
	})
def getMoveMsg(pos):
	return json.dumps({
		"type" : "move",
		"position" : pos
	})
def getTeam():
	while True:
		team = raw_input('choose team: ')
		if team in ["x", "o", "X", "O"]:
			return team.lower()
def getPos():
	while True:
		try:
			pos = int(raw_input("enter a position between 1 and 9: "))
			if 0 < pos <= 9:
				return pos
		except ValueError:
			pass
def onMessage(ws, message):
	j = json.loads(message)

	if j['type'] == "field":
		print j['field'][0], j['field'][1], j['field'][2]
		print j['field'][3], j['field'][4], j['field'][5]
		print j['field'][6], j['field'][7], j['field'][8]
		print ""
	elif j['type'] == "state":
		if j['state'] == "userMove":
			pos = getPos()
			msg = getMoveMsg(pos)
			ws.send(msg)
		elif j['state'] == "aiMove":
			print "aiMove"
		else:
			print j['state']
			ws.close()
	else:
		print j


def onError(ws, error):
	print "Error:", error

def onOpen(ws):
	team = getTeam()
	msg = getLoginMsg(team)
	ws.send(msg)



if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: %s <ws address>" % sys.argv[0]
	else:
		#websocket.enableTrace(True)
		address = "ws://" + sys.argv[1]
		ws = websocket.WebSocketApp(
			address,
			on_message = onMessage,
			on_error = onError,
			on_open = onOpen
		)
		ws.run_forever()
