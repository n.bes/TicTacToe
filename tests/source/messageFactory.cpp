#include "messageFactory.h"

#include <catch.hpp>
#include <json.hpp>

TEST_CASE("LoginMessage [MessageFactory]"){
	MessageFactory mf;

	SECTION ("good. select X"){
		auto json = R"(
			{
				"type": "login",
				"team": "x"
			}
		)"_json;

		auto rawJson = json.dump();
		auto message = mf.fromString(rawJson);

		auto loginMessage = std::dynamic_pointer_cast<LoginMessage>(message);
		REQUIRE_FALSE(loginMessage == nullptr);
		Team team = loginMessage->getTeam();
		REQUIRE(team == Team::x);
	}
	SECTION ("good. select O"){
		auto json = R"(
			{
				"type": "login",
				"team": "o"
			}
		)"_json;

		auto rawJson = json.dump();
		auto message = mf.fromString(rawJson);

		auto loginMessage = std::dynamic_pointer_cast<LoginMessage>(message);
		REQUIRE_FALSE(loginMessage == nullptr);
		Team team = loginMessage->getTeam();
		REQUIRE(team == Team::o);
	}
	SECTION ("extended"){
		auto json = R"(
			{
				"type": "login",
				"number": 4,
				"team": "x"
			}
		)"_json;

		auto rawJson = json.dump();
		auto message = mf.fromString(rawJson);

		REQUIRE(message->getType() == MessageType::login);
	}
}
TEST_CASE("UnknownMessage [MessageFactory]"){
	MessageFactory mf;

	SECTION ("bad LoginMessage. bad team"){
		auto json = R"(
			{
				"type": "login",
				"number": 4,
				"team": "B"
			}
		)"_json;

		auto rawJson = json.dump();
		auto message = mf.fromString(rawJson);

		REQUIRE(message->getType() == MessageType::unknown);
	}

	SECTION ("bad LoginMessage. type/team level down"){
		auto json = R"(
			{
				"a": {
					"type": "login",
					"team": "X"
				},
				"team": "X"
			}
		)"_json;

		auto rawJson = json.dump();
		auto message = mf.fromString(rawJson);

		REQUIRE(message->getType() == MessageType::unknown);
	}
	SECTION ("garbage"){
		auto garbage = "{4,2,2,)";
		auto message = mf.fromString(garbage);

		REQUIRE(message->getType() == MessageType::unknown);
	}
}
