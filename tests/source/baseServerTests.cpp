#include "baseServer.h"
#include "messageFactory.h"
#include "jsonMessageFactory.h"
#include "linearAi.h"

#include <catch.hpp>
#include <json.hpp>

class TestAi : public AiInterface{
	Position move (Field field){
		return Position::A1;
	}
};


TEST_CASE("ctor [BaseServer]"){
	auto testAi = std::make_shared <TestAi>();

	SECTION("default"){
		BaseServer bs(testAi);
	}
}
TEST_CASE("ctor [json]"){
	auto testAi = std::make_shared <TestAi>();

	BaseServer bs(testAi);
	nlohmann::json json = nlohmann::json::parse("{\"happy\":true}");
	REQUIRE (bs.getUserTeam() == Team::unknown);
	REQUIRE (bs.getAiTeam() == Team::unknown);
}
TEST_CASE("addMessage/getState [BaseServer]"){
	auto testAi = std::make_shared <TestAi>();

	BaseServer bs(testAi);

	SECTION("login. play for X"){
		auto json = R"(
			{
				"type": "login",
				"team": "x"
			}
		)"_json;

		bs.addMessage(json.dump());
		bs.tick();
		REQUIRE (bs.getState() == ServerState::userMove);
		REQUIRE (bs.getUserTeam() == Team::x);
		REQUIRE (bs.getAiTeam() == Team::o);
	}
	SECTION("login. play for O"){
		auto json = R"(
			{
				"type": "login",
				"team": "o"
			}
		)"_json;

		bs.addMessage(json.dump());
		bs.tick();
		REQUIRE (bs.getState() == ServerState::aiMove);
		REQUIRE (bs.getUserTeam() == Team::o);
		REQUIRE (bs.getAiTeam() == Team::x);
	}
	SECTION("login. play for Z"){
		auto json = R"(
			{
				"type": "login",
				"team": "Z"
			}
		)"_json;

		bs.addMessage(json.dump());
		bs.tick();
		REQUIRE (bs.getState() == ServerState::login);
		REQUIRE (bs.getUserTeam() == Team::unknown);
		REQUIRE (bs.getAiTeam() == Team::unknown);
	}
}

TEST_CASE("play with linearBot [BaseServer]"){
	auto linearAi = std::make_shared <LinearAi>();

	BaseServer bs(linearAi);
	JsonMessageFactory jsonMessageFactory;

	//SECTION("nothing")
	{
		REQUIRE (bs.getState() == ServerState::login);
		bs.tick();
		REQUIRE (bs.getState() == ServerState::login);
	}

	//SECTION("login")
	{
		auto loginMessage = jsonMessageFactory.loginMsg(Team::x);
		bs.addMessage(loginMessage.dump());
		bs.tick();
		REQUIRE (bs.getState() == ServerState::userMove);
	}

	//SECTION("move 1")
	{
		auto moveMessage = jsonMessageFactory.moveMsg(Position::A1);
		bs.addMessage(moveMessage.dump());
		bs.tick();
		REQUIRE (bs.getState() == ServerState::aiMove);
		bs.tick();
		REQUIRE (bs.getState() == ServerState::userMove);
	}
	//SECTION("double move 1")
	{
		auto moveMessage = jsonMessageFactory.moveMsg(Position::A1);
		bs.addMessage(moveMessage.dump());
		bs.tick();
		REQUIRE (bs.getState() == ServerState::userMove);
	}

	//SECTION("move 2")
	{
		auto moveMessage = jsonMessageFactory.moveMsg(Position::A3);
		bs.addMessage(moveMessage.dump());
		bs.tick();
		REQUIRE (bs.getState() == ServerState::aiMove);
		bs.tick();
		REQUIRE (bs.getState() == ServerState::userMove);
	}

	//SECTION("move 3")
	{
		auto moveMessage = jsonMessageFactory.moveMsg(Position::B2);
		bs.addMessage(moveMessage.dump());
		bs.tick();
		REQUIRE (bs.getState() == ServerState::aiMove);
		bs.tick();
		REQUIRE (bs.getState() == ServerState::userMove);
	}

	//SECTION("move 4")
	{
		auto moveMessage = jsonMessageFactory.moveMsg(Position::C2);
		bs.addMessage(moveMessage.dump());
		bs.tick();
		REQUIRE (bs.getState() == ServerState::aiMove);
		bs.tick();
		REQUIRE (bs.getState() == ServerState::userMove);
	}

	//SECTION("gameOver")
	{
		auto moveMessage = jsonMessageFactory.moveMsg(Position::C3);
		bs.addMessage(moveMessage.dump());
		bs.tick();
		REQUIRE (bs.getState() == ServerState::gameOver);
		bs.tick();
		REQUIRE (bs.getState() == ServerState::gameOver);
	}
}

TEST_CASE("fast game with linearBot. Play for 'x'. [BaseServer]"){
	auto linearAi = std::make_shared <LinearAi>();

	BaseServer bs(linearAi);
	JsonMessageFactory jsonMessageFactory;

	auto loginMessage = jsonMessageFactory.loginMsg(Team::x);
	auto moveMsgToB1 = jsonMessageFactory.moveMsg(Position::B1);
	auto moveMsgToB2 = jsonMessageFactory.moveMsg(Position::B2);
	auto moveMsgToB3 = jsonMessageFactory.moveMsg(Position::B3);

	bs.addMessage(loginMessage.dump());
	bs.addMessage(moveMsgToB1.dump());
	bs.addMessage(moveMsgToB2.dump());
	bs.addMessage(moveMsgToB3.dump());

	REQUIRE (bs.getState() == ServerState::login);
	bs.tick();
	REQUIRE (bs.getState() == ServerState::userMove);
	bs.tick();
	REQUIRE (bs.getState() == ServerState::aiMove);
	bs.tick();
	REQUIRE (bs.getState() == ServerState::userMove);
	bs.tick();
	REQUIRE (bs.getState() == ServerState::aiMove);
	bs.tick();
	REQUIRE (bs.getState() == ServerState::userMove);
	bs.tick();
	REQUIRE (bs.getState() == ServerState::gameOver);
}
TEST_CASE("fast game with linearBot. Play for 'o'. [BaseServer]"){
	auto linearAi = std::make_shared <LinearAi>();

	BaseServer bs(linearAi);
	JsonMessageFactory jsonMessageFactory;

	auto loginMessage = jsonMessageFactory.loginMsg(Team::o);
	auto moveMsgToB1 = jsonMessageFactory.moveMsg(Position::B1);
	auto moveMsgToB2 = jsonMessageFactory.moveMsg(Position::B2);

	bs.addMessage(loginMessage.dump());
	bs.addMessage(moveMsgToB1.dump());
	bs.addMessage(moveMsgToB2.dump());

	REQUIRE (bs.getState() == ServerState::login);
	bs.tick();
	REQUIRE (bs.getState() == ServerState::aiMove);
	bs.tick();
	REQUIRE (bs.getState() == ServerState::userMove);
	bs.tick();
	REQUIRE (bs.getState() == ServerState::aiMove);
	bs.tick();
	REQUIRE (bs.getState() == ServerState::userMove);
	bs.tick();
	REQUIRE (bs.getState() == ServerState::aiMove);
	bs.tick();
	REQUIRE (bs.getState() == ServerState::gameOver);
}
