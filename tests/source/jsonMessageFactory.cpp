#include "jsonMessageFactory.h"
#include "field.h"

#include <catch.hpp>
#include <json.hpp>


TEST_CASE("field [jsonMessageFactory]"){
	JsonMessageFactory jsonMessageFactory;

	SECTION ("default"){
		auto json = R"(
			{
				"type": "field",
				"field": ["-", "-", "-", "-", "-", "-", "-", "-", "-"]
			}
		)"_json;

		Field f;
		auto msg = jsonMessageFactory.fieldMsg(f);
		REQUIRE(json == msg);
	}
	SECTION ("set x to A2"){
		auto json = R"(
			{
				"type": "field",
				"field": ["-", "x", "-", "-", "-", "-", "-", "-", "-"]
			}
		)"_json;

		Field f;
		f.set(Team::x, Position::A2);
		auto msg = jsonMessageFactory.fieldMsg(f);
		REQUIRE(json == msg);
	}
	SECTION ("set o to A2"){
		auto json = R"(
			{
				"type": "field",
				"field": ["-", "o", "-", "-", "-", "-", "-", "-", "-"]
			}
		)"_json;

		Field f;
		f.set(Team::o, Position::A2);
		auto msg = jsonMessageFactory.fieldMsg(f);
		REQUIRE(json == msg);
	}
}


TEST_CASE("loginMsg [jsonMessageFactory]"){

}

TEST_CASE("moveMsg [jsonMessageFactory]"){

}
