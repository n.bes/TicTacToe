from bottle import route, run, static_file
import os

def getCurrentDir():
	pathToFile = os.path.realpath(__file__)
	return os.path.dirname(pathToFile)

@route('/<any>')
def hello(any):
	root = getCurrentDir()
	return static_file("client.html", root=root)

@route('/')
def hello(any=""):
	root = getCurrentDir()
	return static_file("client.html", root=root)

run(host='0.0.0.0', port=80)
