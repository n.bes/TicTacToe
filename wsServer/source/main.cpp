#define ELPP_THREAD_SAFE
#include "baseServer.h"
#include "linearAi.h"

#include <server_ws.hpp>
#include <easylogging++.h>

#include <chrono>
#include <iostream>
#include <map>
#include <atomic>
#include <mutex>
#include <string>
#include <thread>

INITIALIZE_EASYLOGGINGPP

int main() {
	typedef SimpleWeb::SocketServer<SimpleWeb::WS> WsServer;
	const int threadCount = 4;
	const int port = 8080;

	std::map <std::shared_ptr <WsServer::Connection>, std::shared_ptr<BaseServer>> gameMap;
	std::mutex mutex;

	WsServer wsServer(port, threadCount);
	auto& ticTacToeServer = wsServer.endpoint["/"];

	ticTacToeServer.onopen = [&] (std::shared_ptr<WsServer::Connection> connection) {
		auto linearAi = std::make_shared <LinearAi>();
		auto newServer = std::make_shared<BaseServer>(linearAi);

		newServer->onMsg.connect([&wsServer, connection] (std::string msg){
			LOG(INFO) << "[" << (size_t)connection.get() << "] send: " << msg;

			auto stream = std::make_shared<WsServer::SendStream>();
			*stream << msg;

			wsServer.send(connection, stream, [&](const boost::system::error_code& ec){
				if(ec) {
					LOG(ERROR) << "Server: Error sending message. " << "Error: " << ec << ", error message: " << ec.message();
				}
			});
		});

		auto newClient = std::pair<std::shared_ptr<WsServer::Connection>, std::shared_ptr<BaseServer>> (connection, newServer);
		std::lock_guard<std::mutex> lock(mutex);
		gameMap.insert(newClient);

		LOG(INFO) << "[" << (size_t)connection.get() << "] added";
	};

	ticTacToeServer.onmessage = [&](std::shared_ptr<WsServer::Connection> connection, std::shared_ptr<WsServer::Message> message){
		const auto game = gameMap.find(connection);

		if (game != gameMap.end()){
			auto strMsg = message->string();
			game->second->addMessage(strMsg);

			LOG(INFO) << "[" << (size_t)connection.get() << "] receive: " << strMsg;
		}
	};

	ticTacToeServer.onclose = [&](std::shared_ptr<WsServer::Connection> connection, int status, const std::string& reason) {
		std::lock_guard<std::mutex> lock(mutex);
		gameMap.erase (connection);

		LOG(INFO) << "[" << (size_t)connection.get() << "]" << " removed";
	};

	ticTacToeServer.onerror = [&](std::shared_ptr<WsServer::Connection> connection, const boost::system::error_code& ec) {
		std::lock_guard<std::mutex> lock(mutex);
		gameMap.erase (connection);

		LOG(INFO) << "[" << (size_t)connection.get() << "]" << " removed. Error:" << ec.message();
	};

	std::atomic_bool run = { true };
	std::vector <std::thread> threads;
	for (int i = 0; i < threadCount; ++i){
		std::thread thread([&](){
			while (run){
				std::this_thread::sleep_for(std::chrono::nanoseconds(1));
				std::lock_guard<std::mutex> lock(mutex);
				for (const auto &kv: gameMap){
					kv.second->tick();
				}
			}
		});
		threads.push_back(std::move(thread));
	}
	LOG(INFO) << "Listening...";

	wsServer.start();

	run = false;
	wsServer.stop();
	for (auto &t: threads){
		t.join();
	}

	return 0;
}
